function InputHint(inputElement, labelElement) {
  this.inputElement = inputElement;
  this.labelElement = labelElement;
  this.labelText = labelElement.text();
}

InputHint.prototype.init = function() {
  this.setDefaultInput();
  this.labelRemove();
  this.bindEvents();
  ///this.focusEvent();
  ///this.blurEvent();
};

InputHint.prototype.bindEvents = function() {
  this.inputElement.focus(this.focusEvent.bind(this));
  this.inputElement.blur(this.blurEvent.bind(this));
}
// 1. Set the value of the search input to the text of the label element.
InputHint.prototype.setDefaultInput = function() {
  this.inputElement.val(this.labelText);
// 2. Add a class of "hint" to the search input
  this.inputElement.addClass("hint");
};

// 3. Remove the label element
InputHint.prototype.labelRemove = function() {
  this.labelElement.remove();
};

// 4. Bind a focus event to the search input that removes the hint text and the "hint" class
InputHint.prototype.focusEvent = function() {
  this.inputElement.val("");
  this.inputElement.removeClass('hint');
};

// 5. Bind a blur event to the search input that restores the hint text and "hint" class if no search text was entered
InputHint.prototype.blurEvent = function() {
  if(this.inputElement.val() == "") {
    this.inputElement.val(labelText);
    this.inputElement.addClass("hint");
  }
};

window.onload = function() {
  var inputElement = $("#search input.input_text"),
      labelElement = $("#search label"),
      inputHint = new InputHint(inputElement, labelElement);
  inputHint.init();
};